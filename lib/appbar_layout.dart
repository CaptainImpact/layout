import 'package:flutter/material.dart';
import 'package:layout_building/colors.dart';

class AppBarLayout extends StatelessWidget with PreferredSizeWidget {
  final Gradient gradient;
  final Widget child;
  final Size size;
  final bool isMain;

  AppBarLayout({this.gradient, @required this.child, this.size, this.isMain = false});

  @override
  Size get preferredSize => Size(double.infinity, isMain ? 140 : 70);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: gradient ?? LinearGradient(
          colors: [
            MARIGOLD,
            WHEAT,
          ],
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          stops: [0.1, 0.9],
          tileMode: TileMode.clamp,
        ),
      ),
      height: preferredSize.height,
      width: preferredSize.width,
      child: child,
    );
  }
}
