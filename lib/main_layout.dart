import 'package:flutter/material.dart';
import 'package:layout_building/appbar_layout.dart';
import 'package:layout_building/colors.dart';

class MainLayout extends StatelessWidget {
  final double padding;
  final bool hasBottomBar;
  final bool resizeToAvoidBottomPadding;
  final PreferredSizeWidget appBar;
  final Color bgColor;
  final Widget child;

  MainLayout({
    @required this.child,
    this.appBar,
    this.bgColor = Colors.white,
    this.resizeToAvoidBottomPadding = false,
    this.padding = 16.0,
    this.hasBottomBar = true,
  }) : assert(child != null, throw ('Child must be initialize'));

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        resizeToAvoidBottomPadding: resizeToAvoidBottomPadding,
        appBar: appBar ?? BlankAppBar(),
        backgroundColor: Colors.white,
       bottomNavigationBar: hasBottomBar ? BottomNavigationBar(
         items: [
           BottomNavigationBarItem(icon: Icon(Icons.add),),
           BottomNavigationBarItem(icon: Icon(Icons.remove),),
         ],
       ) : const SizedBox(),
        body: Directionality(
          textDirection: TextDirection.ltr,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: padding),
            width: double.infinity,
            height: double.infinity,
            child: child,
          ),
        ),
      ),
    );
  }
}

class BlankAppBar extends StatelessWidget with PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBarLayout(
      gradient: null,
      child: null,
      size: preferredSize,
    );
  }

  @override
  Size get preferredSize => Size(double.infinity, 0);
}



