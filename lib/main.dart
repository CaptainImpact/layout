import 'package:flutter/material.dart';
import 'package:layout_building/appbar_layout.dart';
import 'package:layout_building/main_layout.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Page1(title: 'Flutter Demo Home Page'),
    );
  }
}

class Page1 extends StatefulWidget {
  Page1({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _Page1State createState() => _Page1State();
}

class _Page1State extends State<Page1> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
      allowFontScaling: false,
    );
    return MainLayout(
      appBar: AppBar(
        title: AppBarLayout(
          child: Text('Wasd'),
          isMain: true,
        ),
      ),
      child: InkWell(
        onTap: () {},
        child: Container(
          height: 90.h,
          width: 160.w,
          child: Text(
            widget.title,
            style: TextStyle(fontSize: 99.sh),
          ),
        ),
      ),
    );
  }
}
